﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace helloworld12345
{
    class Dinosaur
    {
        //four things are required to show case a dinosaur.
        //id
        public int DinoId { set; get; }
        //name
        public string DinoName { set; get; }
        //height
        public int DinoHeight { set; get; }
        //weight
        public int DinoWeight { set; get; }
        //terrain
        public string DinoTerrain { set; get; }

        //TODO

        //default constructor

        public Dinosaur()
        {
            DinoId = 01;
            DinoName = "some name";
            DinoHeight = 100;
            DinoWeight = 100;
            DinoTerrain = "IN";
        }
        //custom constructor with parameters
        public Dinosaur(int DinoId,string DinoName,int DinoHeight,int DinoWeight,string DinoTerrain)
        {
//this.DinoId = DinoId;
            this.DinoName = DinoName;
            this.DinoHeight = DinoHeight;
            this.DinoWeight = DinoWeight;
            this.DinoTerrain = DinoTerrain;
            this.DinoId = DinoId;
        }

        //function do take input for Dino
        public Dinosaur input()
        {
            Console.WriteLine("enter Dino ID");
            DinoId = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter Dino name");
            DinoName = Console.ReadLine();
            Console.WriteLine("enter Dino height");
            DinoHeight = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter Dino weight");
            DinoWeight = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter Dino terrain");
            DinoTerrain = Console.ReadLine();

            var toreturnDino = new Dinosaur(DinoId,DinoName, DinoHeight, DinoWeight, DinoTerrain);
            return toreturnDino;

        }

        //function to display Dino

        public void  display()
        { 

            Console.WriteLine("Dino Id: " + DinoId );
            Console.WriteLine("Dino Name: " + DinoName );
            Console.WriteLine("Dino Height: "+ DinoHeight);
            Console.WriteLine("Dino wight: "+DinoWeight);
            Console.WriteLine("Dino terrain: "+DinoTerrain);

        }

        //function to print all Dino Names
    }
}
