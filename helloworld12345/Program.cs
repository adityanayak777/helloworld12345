﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace helloworld12345
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");


            #region Dino Stuff

            Console.WriteLine("---------------------DINO STUFF BEGINS ---------------------");

            //create a basic dino object.
            var tempDinosaur1 = new Dinosaur();

            var tempDinoString = tempDinosaur1.ToString();

            Console.WriteLine(tempDinoString);

//object 2- custom parameter
           var tempDinosaur2 = new Dinosaur(2,"Triceratops", 10,100,"IN");
            var tempDinosaur3 = new Dinosaur(3, "Theropods", 30, 100, "IN");
            var tempDinosaur4 = new Dinosaur(4, "Sauropoda", 20, 100, "AM");
            var tempDinosaur5 = new Dinosaur(5, "Troodon", 50, 100, "AM");
            var tempDinosaur6 = new Dinosaur(6, "Troodon", 70, 100, "AM");

//var tempDinosaur6 = new Dinosaur(7, "Troodon", 100, 100, "water");

            var tempDinosaur7 = new Dinosaur(8, "Troodon", 60, 100, "CH");

            var tempDinosaur8 = new Dinosaur(9, "Troodon", 40, 100, "CH");

            var tempDinosaur9= new Dinosaur(10, "Troodon", 100, 100, "CH");
            var tempDinosaur10 = new Dinosaur(11, "Troodon", 90, 100, "CH");



            var tempDinoString2 = tempDinosaur2.ToString();

            Console.WriteLine(tempDinoString2);
//object3
      /*      var tempDinosaur3 = new Dinosaur();
            tempDinosaur3 =tempDinosaur3.input();
            tempDinosaur3.display();*/

            var CollectionOfDino = new List<Dinosaur>();

            CollectionOfDino.Add(tempDinosaur1);
            CollectionOfDino.Add(tempDinosaur2);
            CollectionOfDino.Add(tempDinosaur3);
            CollectionOfDino.Add(tempDinosaur4);
            CollectionOfDino.Add(tempDinosaur5);
            CollectionOfDino.Add(tempDinosaur6);
            CollectionOfDino.Add(tempDinosaur7);
            CollectionOfDino.Add(tempDinosaur8);
            CollectionOfDino.Add(tempDinosaur9);
            CollectionOfDino.Add(tempDinosaur10);

            var collectionOfDino_Sorted = CollectionOfDino.OrderByDescending(x=>x.DinoHeight).ToList();
            // CollectionOfDino.Add(tempDinosaur3);

            DisplayDinoCollection(collectionOfDino_Sorted);

            showdinobasedoncountry(CollectionOfDino);
         //   var code=Console.ReadLine();
            searchdinobasedoncountry(CollectionOfDino);
            #endregion

        //    searchdinobasedonid(CollectionOfDino);
            //  DinoInputFiveDinos(CollectionOfDino);
            //DisplayDinoCollection(CollectionOfDino);
            DeleteDino(CollectionOfDino);

            #region basic function stuff

            var name = "aditya";
            f1();
            f2(name);
            var message = f3(name);
            Console.WriteLine("In main function " + message);

            #endregion


            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }

        private static void searchdinobasedonid(List<Dinosaur> collectionOfDino)
        {
             Console.WriteLine("enter the id:");
            var id = Convert.ToInt32(Console.ReadLine());
            var count = collectionOfDino.Count();
            if(id <= count)
            {
                id = count + 1;

            }

        }
        private static void DeleteDino(List<Dinosaur> collectionOfDino)
        {
            var message = "";

            //display message 
            message = "Enter a id and we will delete that dino.";
            Console.WriteLine(message);
            var enteredDinoID = Console.ReadLine();
            var DinoIDNumber = Convert.ToInt32(enteredDinoID);

            //find the dino with the entered id. 
            var DinoFound = collectionOfDino.Select(x => x).Where(x => x.DinoId== DinoIDNumber).FirstOrDefault();

            if (DinoFound == null)
            {
                //display message 
                message = "Dino with ID - " + DinoIDNumber + "does not exist in our collection";
                Console.WriteLine(message);
            }
            else
            {
                collectionOfDino.Remove(DinoFound);
                message = "Dino with ID - " + DinoIDNumber + "has been deleted";
                Console.WriteLine(message);
            }

            DisplayDinoCollection(collectionOfDino);
        }
    /*  private static void DinoInputFiveDinos(List<Dinosaur> collectionOfDino)
        {
            int numberOfDinosToEnter = 2;
            //throw new NotImplementedException();
            var message = "";

            //display message 
            message = "Now, time to add " + numberOfDinosToEnter + "dinos to the collection.";
            Console.WriteLine(message);

            for (int i = 0; i < numberOfDinosToEnter; i++)
            {

                //display message 
                message = "Add dino details ";
                Console.WriteLine(message);

                //lets do the input
                //some temporary values to which we will fill user entered values.
                string tempDinoName = "Triceratops";
                int tempDinoHeight = 10;
                int tempDinoWeight = 12;
                string tempDinoTerrain = "North America";
                int tempDinoID = 1;

                //collect input from user


                message = "enter dino name";
                Console.WriteLine(message);
                tempDinoName = Console.ReadLine();

                message = "enter dino height";
                Console.WriteLine(message);
                var tempNumber = Console.ReadLine();
                tempDinoHeight = Convert.ToInt32(tempNumber);

                message = "enter dino weight";
                Console.WriteLine(message);
                var tempNumber2 = Console.ReadLine();
                tempDinoWeight = Convert.ToInt32(tempNumber2);

                message = "enter dino terrain";
                Console.WriteLine(message);
                tempDinoTerrain = Console.ReadLine();

                message = "enter dino id";
                Console.WriteLine(message);
                var tempNumber3 = Console.ReadLine();
                tempDinoID = Convert.ToInt32(tempNumber3);

                //check if ID is unique. 
                var FindDino = collectionOfDino.Select(x => x).Where(x => x.DinoId == tempDinoID).FirstOrDefault();

                if (FindDino != null)
                {
                    //this means, dino with that id is already in our collection. 
                    //get the id of the last dino in our collection.
                    var lastDino = collectionOfDino.Last();
                    //get the id of the last dino
                    var lastDinoID = lastDino.DinoId;
                    //increase this by 1 and make it the id of the new dino.

                    //fill it up to a dino object.
                    //create a dino object to return after getting all the values from the user. 

                    var toReturnDino = new Dinosaur(tempDinoID, tempDinoName,tempDinoHeight,tempDinoWeight,tempDinoTerrain);

                    //add this new dino to our collection. 
                    collectionOfDino.Add(toReturnDino);

                    //display message 
                    message = "Dino with ID " + toReturnDino.DinoId + " added to our collection";
                    Console.WriteLine(message);
                }//end of for loop

                //return the collection with the newly added dinos.
                return collectionOfDino;
               // tempDinoID = lastDinoID + 1;
            }

        }
*/
        

        private static void searchdinobasedoncountry(List<Dinosaur> collectionOfDino)
        {
            Console.WriteLine("Enter country code:");

            var countrycode = Console.ReadLine();

            //var countrycode = regioncode;
            var DinoResultList = collectionOfDino.Select(x => x).Where(x => x.DinoTerrain == countrycode).ToList();

            DisplayDinoCollection(DinoResultList);
        }

        private static void showdinobasedoncountry(List<Dinosaur> collectionOfDino)
        {
            var dinoindia = collectionOfDino.Select(x => x.DinoTerrain == "IN").ToList();
            var dinoamerica = collectionOfDino.Select(x => x.DinoTerrain == "AM").ToList();
            var dinochina = collectionOfDino.Select(x => x.DinoTerrain == "CH").ToList();

            
                var dinoindialist = collectionOfDino.Select(x => x).Where(x => x.DinoTerrain == "IN").ToList();
            var dinoamericalist = collectionOfDino.Select(x => x).Where(x => x.DinoTerrain == "AM").ToList();
            var dinochinalist = collectionOfDino.Select(x => x).Where(x => x.DinoTerrain == "CH").ToList();
            DisplayDinoCollection(dinoindialist);
              DisplayDinoCollection(dinoamericalist);
            DisplayDinoCollection(dinochinalist);
           //var somthing = 0;

        }


        private static void DisplayDinoCollection(List<Dinosaur> collectionOfDino)
        {
            //throw new NotImplementedException();

            //get total dinos.
            var totalDinos = collectionOfDino.Count;
              //  var temp1= collectionOfDino.

            var message = "";

            //display total dinos for reference
            message = "Total Number of Dinos - " + totalDinos;
            Console.WriteLine(message);

            //loop through each dino.
            foreach (var dino in collectionOfDino)
            {
                //display dino details using the already existing display function
                dino.display();

                //put a simple line to indicate that a new dino will be displated in the next iteration
                message = "--------------------";
                Console.WriteLine(message);
            }
        }
        static void BasicTypeStuff()
        {


            #region basic int stuff

            //int a = 5;
            //int b = a + 2; //OK

            //bool test = true;

            //// Error. Operator '+' cannot be applied to operands of type 'int' and 'bool'.
            //int c = a + test;

            // Keep the console window open in debug mode.

            #endregion

            #region basic string stuff

            String string1 = "Exciting times ";
            String string2 = "lie ahead of us";

            String combineTheTwoStrings = string1 + string2;

            Console.WriteLine(combineTheTwoStrings);

            #endregion

            #region basic bool stuff. 

            //taking input
            Console.WriteLine("Enter a number please");
            //var input = Console.ReadLine().ToString();
            var input = "5";

            //converting the input (it will be in string format) to a int type

            int number = 0;
            try
            {
                number = Convert.ToInt32(input);
            }
            catch (Exception e)
            {
                Console.WriteLine("Got some error - {0}", e.ToString());
                //assign a default number in case of error to resume code flow
                number = 10;
            }


            //we need a bool flag. 
            bool flag;

            if (number > 5)
            {
                flag = true;
            }
            else
            {
                flag = false;
            }

            //lets display the value of the flag in the output.
            Console.WriteLine("The value of flag is {0}", flag);

            #endregion

        }

        //first scenario function
        //one way communication without any parameters
        static void f1()
        {
            var message = "I am in function 1";
            Console.WriteLine(message);
        }

        //one way communication with parameters
        static void f2(string name)
        {
            var message = "Helo " + name + ", I am in function 2";
            Console.WriteLine(message);
        }

        //two way communication with parameters. 
        static string f3(string name)
        {
            var message = "Helo " + name + ", I am in function 3";
            //Console.WriteLine(message);
            return message;
        }
    
    }
}
